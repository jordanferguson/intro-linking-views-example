# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
Rails.application.routes.draw do

  # Set homepage
  root 'team#index'

  # Get team pages
  get '/team/htmelodies', to: 'team#htmelodies'
  get '/team/gamehub', to: 'team#gamehub'
  get '/team/findjobs', to: 'team#findjobs'
  get '/team/bookhunter', to: 'team#bookhunter'

  # Only use index and show action
  resources :team, only: [:index, :show]

end
